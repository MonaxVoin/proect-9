let arr = [1, 3, 3, 4, 2, 5, 8, 15];

const maxNumber = Math.min(...arr);// разбор массива, спред, Деструктуризация массива

console.log(maxNumber);


// round - округляет до целого по правилам математики
// floor - округляет до целого в меньшую сторону
//ceil - округляет до целого в большую строну (ceil - потолок)
//random - генерирует псевдо случаное число от 0 до 1


// Math.pow(3.10);
const random = Math.random();

console.log(random);


function getRandom(min, max) {
    let random = min - 0.5 + Math.random() * (max - min + 1);
    return random;
}



console.log(getRandom(10, 100));




// setTimeout выполняеться единожды, setInterval- выполняется постоянно с задданным интервалом 


const timer = setTimeout(sayHello,2000);  //clearTimeout, clearInterval()   //все в милисекундах 2000mc => 2sec, // event loop почитать!!!

clearInterval(timer); // останавливает интервал


function sayHello() {
    console.log('Hello');
}